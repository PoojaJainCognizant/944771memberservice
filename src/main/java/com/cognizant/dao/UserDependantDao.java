package com.cognizant.dao;

import java.util.List;

public class UserDependantDao {

	private String userId;
	private List<DependantDao> dependants;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<DependantDao> getDependants() {
		return dependants;
	}

	public void setDependants(List<DependantDao> dependants) {
		this.dependants = dependants;
	}

}
