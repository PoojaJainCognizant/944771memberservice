package com.cognizant.dao;

import java.util.List;

public class CountryDao {
	
	private String name;
	private List<String> states;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}
	
}
