package com.cognizant.dao;

import java.util.Date;

public class ClaimDao {

	private String dependantId;
	private Date dateOfAdmission;
	private Date dateOfDischarge;
	private String providerName;
	private Integer billAmt;

	public String getDependantId() {
		return dependantId;
	}

	public void setDependantId(String dependantId) {
		this.dependantId = dependantId;
	}

	public Date getDateOfAdmission() {
		return dateOfAdmission;
	}

	public void setDateOfAdmission(Date dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}

	public Date getDateOfDischarge() {
		return dateOfDischarge;
	}

	public void setDateOfDischarge(Date dateOfDischarge) {
		this.dateOfDischarge = dateOfDischarge;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Integer getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(Integer billAmt) {
		this.billAmt = billAmt;
	}

}
