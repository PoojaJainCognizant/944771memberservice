package com.cognizant.configuration;

import org.springframework.stereotype.Component;

@Component
public class GenerateResponse {

	public ResponseObject genResponse(Object obj, String msg, Integer httpStatus) {
		ResponseObject resObj = new ResponseObject();
		resObj.setData(obj);
		resObj.setMessage(msg);
		resObj.setHttpStatus(httpStatus);
		return resObj;
	}
}
