package com.cognizant.configuration;

public class ResponseObject {
	
	private Object data;
	private String message;
	private Integer httpStatus;
	
	public ResponseObject() {}
	
	public ResponseObject(Object data, String msg, Integer status) {
		this.data = data;
		this.message = msg;
		this.httpStatus = status;
	}
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}
	
}
