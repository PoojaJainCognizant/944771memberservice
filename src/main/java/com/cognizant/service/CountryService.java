package com.cognizant.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.dao.CountryDao;
import com.cognizant.model.Country;
import com.cognizant.model.State;
import com.cognizant.repository.CountryRepository;
import com.cognizant.repository.StateRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepo;

	@Autowired
	private StateRepository stateRepo;

	public Country saveCountry(CountryDao countryDao) {
		Country country = new Country();
		country.setName(countryDao.getName());
		countryRepo.insert(country);
		Set<State> states = new HashSet<>();
		for (String state : countryDao.getStates()) {
			State stateObj = new State();
			stateObj.setName(state);
			stateObj.setCountry(country);
			states.add(stateObj);
		}
		stateRepo.insert(states);
		country.setStates(states);
		countryRepo.save(country);
		return country;
	}

	public List<HashMap<String, Object>> getAllCountries() {
		List<Country> countries = countryRepo.findAll();
		List<HashMap<String, Object>> data = new ArrayList<>();
		for (Country country : countries) {
			HashMap<String, Object> countryData = new HashMap<>();
			countryData.put("id", country.getId());
			countryData.put("name", country.getName());
			data.add(countryData);
		}
		return data;
	}

	public Country getCountry(String id) {
		Country country = countryRepo.findById(id).get();
		return country;
	}

}
