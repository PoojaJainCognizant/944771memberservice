package com.cognizant.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.dao.ClaimDao;
import com.cognizant.dao.DependantDao;
import com.cognizant.dao.LoginDao;
import com.cognizant.dao.NewLoginDao;
import com.cognizant.dao.RegsiterUserDao;
import com.cognizant.dao.UpdateMemberDao;
import com.cognizant.dao.UserDependantDao;
import com.cognizant.exceptions.EntityNotFoundException;
import com.cognizant.model.Claim;
import com.cognizant.model.Country;
import com.cognizant.model.Dependant;
import com.cognizant.model.State;
import com.cognizant.model.User;
import com.cognizant.repository.ClaimRepository;
import com.cognizant.repository.CountryRepository;
import com.cognizant.repository.DependantRepository;
import com.cognizant.repository.StateRepository;
import com.cognizant.repository.UserRepository;
import com.cognizant.vm.ClaimVM;
import com.cognizant.vm.UserVM;

@Service
public class MemberService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private CountryRepository countryRepo;

	@Autowired
	private StateRepository stateRepo;

	@Autowired
	private DependantRepository depRepo;

	@Autowired
	private ClaimRepository claimRepo;

	public UserVM registerNewUser(RegsiterUserDao regDao) throws EntityNotFoundException {
		Optional<Country> countryOpt = countryRepo.findById(regDao.getCountryId());
		if (!countryOpt.isPresent()) {
			throw new EntityNotFoundException("Failed to get Country of provided Id: " + regDao.getCountryId());
		}
		Country country = countryOpt.get();
		Optional<State> stateOpt = stateRepo.findById(regDao.getStateId());
		State state = stateOpt.get();
		User newUser = new User(regDao.getFirstName(), regDao.getLastName(), regDao.getAddress(), country, state,
				regDao.getEmail(), regDao.getPan(), regDao.getContactNo(), regDao.getDob(), true);
		newUser = userRepo.insert(newUser);
		newUser.setUsername(regDao.getEmail());
		userRepo.save(newUser);
		UserVM userVm = new UserVM();
		userVm.setUserId(newUser.getId());
		userVm.setFirstName(newUser.getFirstName());
		userVm.setLastName(newUser.getLastName());
		userVm.setAddress(newUser.getAddress());
		userVm.setCountry(newUser.getCountry());
		userVm.setState(newUser.getState());
		userVm.setEmail(newUser.getEmail());
		userVm.setPan(newUser.getPan());
		userVm.setContactNo(newUser.getContactNo());
		userVm.setDob(newUser.getDateOfBirth());
		return userVm;
	}

	public UserVM getUserDetails(String userId) {
		Optional<User> userOpt = userRepo.findById(userId);
		User user = userOpt.get();
		UserVM userVm = new UserVM();
		userVm.setUserId(user.getId());
		userVm.setFirstName(user.getFirstName());
		userVm.setLastName(user.getLastName());
		userVm.setAddress(user.getAddress());
		userVm.setCountry(user.getCountry());
		userVm.setState(user.getState());
		userVm.setEmail(user.getEmail());
		userVm.setPan(user.getPan());
		userVm.setContactNo(user.getContactNo());
		userVm.setDob(user.getDateOfBirth());
		if (user.getDependants() != null) {
			if (!user.getDependants().isEmpty()) {
				userVm.setHasDependants(true);
			}
		} else {
			userVm.setHasDependants(false);
		}
		return userVm;
	}

	public HashMap<String, Object> updateUserDetails(String userId, UpdateMemberDao updateMemDao) {
		User user = userRepo.findById(userId).get();
		user.setFirstName(updateMemDao.getFirstName());
		user.setLastName(updateMemDao.getLastName());
		user.setAddress(updateMemDao.getAddress());
		user.setEmail(updateMemDao.getEmail());
		Country country = countryRepo.findById(updateMemDao.getCountryId()).get();
		user.setCountry(country);
		State state = stateRepo.findById(updateMemDao.getStateId()).get();
		user.setState(state);
		user.setContactNo(updateMemDao.getContactNo());
		Date dob = new Date(updateMemDao.getDob());
		user.setDateOfBirth(dob);
		userRepo.save(user);
		HashMap<String, Object> data = new HashMap<>();
		data.put("userId", user.getId());
		return data;
	}

	public HashMap<String, Object> createUserDependant(UserDependantDao userDepDao) {
		Optional<User> userOpt = userRepo.findById(userDepDao.getUserId());
		User user = userOpt.get();
		Set<Dependant> dependants = new HashSet<>();
		for (DependantDao depDao : userDepDao.getDependants()) {
			Dependant dependant = new Dependant();
			dependant.setFirstName(depDao.getFirstName());
			dependant.setLastName(depDao.getLastName());
			dependant.setDob(depDao.getDob());
			dependant.setUser(user);
			dependants.add(dependant);
		}
		depRepo.insert(dependants);
		Set<Dependant> userDependants = null;
		if (user.getDependants() == null) {
			userDependants = new HashSet<>();
		} else {
			userDependants = user.getDependants();
		}
		userDependants.addAll(dependants);
		user.setDependants(userDependants);
		userRepo.save(user);
		HashMap<String, Object> map = new HashMap<>();
		map.put("userId", user.getId());
		return map;
	}

	public HashMap<String, Object> getUserDependants(String id) {
		Optional<User> userOpt = userRepo.findById(id);
		User user = userOpt.get();
		HashMap<String, Object> map = new HashMap<>();
		map.put("userId", user.getId());
		List<HashMap<String, Object>> dependantsList = new ArrayList<>();
		for (Dependant dependant : user.getDependants()) {
			HashMap<String, Object> dependantMap = new HashMap<>();
			dependantMap.put("dependantId", dependant.getId());
			dependantMap.put("firstName", dependant.getFirstName());
			dependantMap.put("lastName", dependant.getLastName());
			dependantMap.put("dob", dependant.getDob());
			List<HashMap<String, Object>> claims = new ArrayList<>();
			if(dependant.getClaims() != null) {
				for(Claim claim : dependant.getClaims()) {
					HashMap<String, Object> claimMap = new HashMap<>();
					claimMap.put("claimId", claim.getId());
					claims.add(claimMap);
				}
			}
			dependantMap.put("claims", claims);
			dependantsList.add(dependantMap);
		}
		map.put("dependants", dependantsList);
		return map;
	}
	
	public Dependant getDependantDetails(String dependantId) {
		Dependant dependant = depRepo.findById(dependantId).get();
		return dependant;
	}
	
	public HashMap<String, Object> updateDependantDetails(String dependantId, DependantDao dependantDao) {
		Dependant dependant = depRepo.findById(dependantId).get();
		dependant.setFirstName(dependantDao.getFirstName());
		dependant.setLastName(dependantDao.getLastName());
		dependant.setDob(dependantDao.getDob());
		depRepo.save(dependant);
		HashMap<String, Object> map = new HashMap<>();
		map.put("dependantId", dependant.getId());
		map.put("userId", dependant.getUser().getId());
		return map;
	}

	public Claim createClaim(ClaimDao claimDao) {
		Dependant dependant = depRepo.findById(claimDao.getDependantId()).get();
		Claim claim = new Claim();
		claim.setDateOfAdmission(claimDao.getDateOfAdmission());
		claim.setDateOfDischarge(claimDao.getDateOfDischarge());
		claim.setProviderName(claimDao.getProviderName());
		claim.setTotalBillAmt(claimDao.getBillAmt());
		claim.setDependant(dependant);
		claimRepo.insert(claim);
		Set<Claim> claims = null;
		if(dependant.getClaims() != null) {
			claims = dependant.getClaims();
		}else {
			claims = new HashSet<>();
		}
		claims.add(claim);
		dependant.setClaims(claims);
		depRepo.save(dependant);
		return claim;

	}

	public HashMap<String, Object> login(LoginDao loginDao) {
		User user = userRepo.findByUsernameAndPassword(loginDao.getUsername(), loginDao.getPassword());
		user.setUsername(loginDao.getUsername());
		user.setPassword(loginDao.getPassword());
		userRepo.save(user);
		HashMap<String, Object> dataMap = new HashMap<>();
		dataMap.put("userId", user.getId());
		// dataMap.put("", value)
		return dataMap;
	}

	public HashMap<String, Object> newLogin(NewLoginDao newLoginDao) {
		User user = userRepo.findById(newLoginDao.getUserId()).get();
		user.setUsername(newLoginDao.getUsername());
		user.setPassword(newLoginDao.getPassword());
		userRepo.save(user);
		HashMap<String, Object> dataMap = new HashMap<>();
		dataMap.put("id", user.getId());
		// dataMap.put(, value)
		return dataMap;
	}

	public HashMap<String, Object> getDependantAllClaims(String dependantId) {
		Dependant dependant = depRepo.findById(dependantId).get();
		HashMap<String, Object> map = new HashMap<>();
		map.put("userId", dependant.getUser().getId());
		HashMap<String, Object> dependantMap = new HashMap<>();
		dependantMap.put("dependantId", dependantId);
		dependantMap.put("firstName", dependant.getFirstName());
		dependantMap.put("lastName", dependant.getLastName());
		dependantMap.put("dob", dependant.getDob());
		map.put("dependant", dependantMap);
		List<ClaimVM> claimsList = new ArrayList<>();
		if(dependant.getClaims() != null) {
			for (Claim claim : dependant.getClaims()) {
				ClaimVM claimVm = new ClaimVM();
				claimVm.setClaimId(claim.getId());
				claimVm.setDateOfAdmission(claim.getDateOfAdmission());
				claimVm.setDateOfDischarge(claim.getDateOfDischarge());
				claimVm.setProviderName(claim.getProviderName());
				claimVm.setTotalBillAmt(claim.getTotalBillAmt());
				claimsList.add(claimVm);
			}	
		}
		map.put("claims", claimsList);
		return map;
	}

	public Claim getClaim(String claimId) {
		Claim claim = claimRepo.findById(claimId).get();
		return claim;
	}

	public String generateUniqueId() {
		Random random = new Random();
		Integer ranId = random.nextInt(900) + 100;
		String id = "R" + ranId.toString();
		return id;
	}
}
