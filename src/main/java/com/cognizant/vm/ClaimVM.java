package com.cognizant.vm;

import java.util.Date;

public class ClaimVM {

//	private String userId;
//	private String dependantId;
	private String claimId;
//	private String firstName;
//	private String lastName;
//	private String dateOfBirth;
	private Date dateOfAdmission;
	private Date dateOfDischarge;
	private String providerName;
	private Integer totalBillAmt;

//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}

//	public String getDependantId() {
//		return dependantId;
//	}
//
//	public void setDependantId(String dependantId) {
//		this.dependantId = dependantId;
//	}

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

//	public String getFirstName() {
//		return firstName;
//	}
//
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//
//	public String getLastName() {
//		return lastName;
//	}
//
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//
//	public String getDateOfBirth() {
//		return dateOfBirth;
//	}
//
//	public void setDateOfBirth(String dateOfBirth) {
//		this.dateOfBirth = dateOfBirth;
//	}

	public Date getDateOfAdmission() {
		return dateOfAdmission;
	}

	public void setDateOfAdmission(Date dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}

	public Date getDateOfDischarge() {
		return dateOfDischarge;
	}

	public void setDateOfDischarge(Date dateOfDischarge) {
		this.dateOfDischarge = dateOfDischarge;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Integer getTotalBillAmt() {
		return totalBillAmt;
	}

	public void setTotalBillAmt(Integer totalBillAmt) {
		this.totalBillAmt = totalBillAmt;
	}

}
