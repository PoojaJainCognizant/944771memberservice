package com.cognizant.vm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cognizant.model.Country;
import com.cognizant.model.State;

public class UserVM {

	private String userId;
	private String firstName;
	private String lastName;
	private String address;
	private Country country;
	private State state;
	private String email;
	private String pan;
	private Long contactNo;
	private Date dob;
	private Boolean hasDependants;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public Long getContactNo() {
		return contactNo;
	}

	public void setContactNo(Long contactNo) {
		this.contactNo = contactNo;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Boolean getHasDependants() {
		return hasDependants;
	}

	public void setHasDependants(Boolean hasDependants) {
		this.hasDependants = hasDependants;
	}

}
