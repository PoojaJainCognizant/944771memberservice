package com.cognizant.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.cognizant.configuration.ResponseObject;

@ControllerAdvice
public class CustomResponseEntityExceptionHandling extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleGenException(Exception ex, WebRequest request) {
		return new ResponseEntity<Object>(
				new ResponseObject(null, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { EntityNotFoundException.class })
	public ResponseEntity<Object> handleEntityNotFoundException(Exception ex, WebRequest request) {
		return new ResponseEntity<Object>(new ResponseObject(null, ex.getMessage(), 1001),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
