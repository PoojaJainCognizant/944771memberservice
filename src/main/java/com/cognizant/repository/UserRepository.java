package com.cognizant.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
	
	//@Query("{$and:[{username:?username},{password:?password}]}")
	User findByUsernameAndPassword(String username, String password);
}
