package com.cognizant.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.model.Dependant;

@Repository	
public interface DependantRepository extends MongoRepository<Dependant, String> {

}
