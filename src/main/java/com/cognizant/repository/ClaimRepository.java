package com.cognizant.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.model.Claim;

@Repository
public interface ClaimRepository extends MongoRepository<Claim, String> {

}
