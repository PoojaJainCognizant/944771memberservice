package com.cognizant.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cognizant.model.State;

public interface StateRepository extends MongoRepository<State, String> {

}
