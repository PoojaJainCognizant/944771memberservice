package com.cognizant.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "claim")
public class Claim {

	@Id
	private String id;
	@Field
	@NotNull
	private Date dateOfAdmission;
	@Field
	@NotNull
	private Date dateOfDischarge;
	@Field
	@NotNull
	private String providerName;
	@Field
	private Integer totalBillAmt;
	@DBRef
	@JsonIgnore
	@Field
	private Dependant dependant;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateOfAdmission() {
		return dateOfAdmission;
	}

	public void setDateOfAdmission(Date dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}

	public Date getDateOfDischarge() {
		return dateOfDischarge;
	}

	public void setDateOfDischarge(Date dateOfDischarge) {
		this.dateOfDischarge = dateOfDischarge;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Integer getTotalBillAmt() {
		return totalBillAmt;
	}

	public void setTotalBillAmt(Integer totalBillAmt) {
		this.totalBillAmt = totalBillAmt;
	}

	public Dependant getDependant() {
		return dependant;
	}

	public void setDependant(Dependant dependant) {
		this.dependant = dependant;
	}

}
