package com.cognizant.model;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "user")
public class User {

	@Id
	private String id;
	@Field
	@NotNull
	private String firstName;
	@Field
	@NotNull
	private String lastName;
	@Field
	@NotNull
	private String address;
	@DBRef
	@Field
	@NotNull
	private Country country;
	@DBRef
	@Field
	@NotNull
	private State state;
	@Field
	@NotNull
	@Indexed(unique=true)
	private String email;
	@Field
	@NotNull
	@Indexed(unique=true)
	private String pan;
	@Field
	@NotNull
	private Long contactNo;
	@Field
	@NotNull
	private Date dateOfBirth;
	@Field
	private String password;
	@Field
	@Indexed(unique=true)
	private String username;
	@Field
	@NotNull
	private Boolean isRegistered = false;
	@DBRef
	@JsonIgnore
	@Field
	private Set<Dependant> dependants;
	
	public User(){}
	
	public User(String firstName,String lastName,String address,Country country,
			State state,String email,String pan,Long contactNo,Date dob,Boolean isRegistered)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.country = country;
		this.state = state;
		this.email = email;
		this.pan = pan;
		this.contactNo = contactNo;
		this.dateOfBirth = dob;
		this.isRegistered = isRegistered;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public Long getContactNo() {
		return contactNo;
	}

	public void setContactNo(Long contactNo) {
		this.contactNo = contactNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public Set<Dependant> getDependants() {
		return dependants;
	}

	public void setDependants(Set<Dependant> dependants) {
		this.dependants = dependants;
	}
	
}
