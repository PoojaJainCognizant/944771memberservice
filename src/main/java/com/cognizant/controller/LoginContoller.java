package com.cognizant.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.dao.LoginDao;
import com.cognizant.dao.NewLoginDao;
import com.cognizant.service.MemberService;

@RestController
@RequestMapping("/api/member")
public class LoginContoller {

	@Autowired
	private MemberService memServ;

	@PostMapping("/login")
	public ResponseEntity<Object> login(@RequestBody LoginDao loginDao) {
		HashMap<String, Object> map = memServ.login(loginDao);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@PostMapping("/login/new")
	public ResponseEntity<Object> newLogin(@RequestBody NewLoginDao newLoginDao) {
		HashMap<String, Object> map = memServ.newLogin(newLoginDao);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}
}
