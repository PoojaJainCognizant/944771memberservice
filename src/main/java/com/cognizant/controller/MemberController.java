package com.cognizant.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.configuration.GenerateResponse;
import com.cognizant.dao.ClaimDao;
import com.cognizant.dao.DependantDao;
import com.cognizant.dao.UpdateMemberDao;
import com.cognizant.dao.UserDependantDao;
import com.cognizant.model.Claim;
import com.cognizant.model.Dependant;
import com.cognizant.service.MemberService;
import com.cognizant.vm.UserVM;

@RestController
@RequestMapping("/api/member")
public class MemberController {

	@Autowired
	private MemberService memServ;

	@Autowired
	private GenerateResponse genRes;

	@GetMapping("/user/{id}")
	public ResponseEntity<Object> getUserDetails(@PathVariable(value = "id") String id, HttpServletRequest request) {
		UserVM user = memServ.getUserDetails(id);
		return new ResponseEntity<Object>(user, HttpStatus.OK);
	}

	@PutMapping("/user/update/{id}")
	public ResponseEntity<Object> updateUserDetails(@PathVariable(value = "id") String userId,
			@RequestBody UpdateMemberDao updateMemDao) {
		HashMap<String, Object> data = memServ.updateUserDetails(userId, updateMemDao);
		return new ResponseEntity<Object>(data, HttpStatus.OK);
	}

	@PostMapping("/user/create-dependants")
	public ResponseEntity<Object> createUserDependants(@RequestBody UserDependantDao userDepDao,
			HttpServletRequest request) throws Exception {
		HashMap<String, Object> map = memServ.createUserDependant(userDepDao);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@GetMapping("/user/get-dependants/{id}")
	public ResponseEntity<Object> getUserDependants(@PathVariable(value = "id") String id, HttpServletRequest request) {
		HashMap<String, Object> map = memServ.getUserDependants(id);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@GetMapping("/user/get-dependant/{id}")
	public ResponseEntity<Object> getDependantDetails(@PathVariable(value = "id") String dependantId) {
		Dependant dependant = memServ.getDependantDetails(dependantId);
		return new ResponseEntity<Object>(genRes.genResponse(dependant, "Success", HttpStatus.OK.value()),
				HttpStatus.OK);
	}

	@PutMapping("/user/update-dependant/{id}")
	public ResponseEntity<Object> updateDependantDetails(@PathVariable(value = "id") String dependantId,
			@RequestBody DependantDao dependantDao) {
		HashMap<String, Object> data = memServ.updateDependantDetails(dependantId, dependantDao);
		return new ResponseEntity<Object>(genRes.genResponse(data, "Updated", HttpStatus.OK.value()), HttpStatus.OK);
	}

	@PostMapping("/user/dependant/create/claim")
	public ResponseEntity<Object> submitClaim(@RequestBody ClaimDao claimDao) {
		Claim claim = memServ.createClaim(claimDao);
		return new ResponseEntity<Object>(claim, HttpStatus.OK);
	}

	@GetMapping("/user/dependant/{id}/get-all-claims")
	public ResponseEntity<Object> getDependantAllClaims(@PathVariable(value = "id") String dependantId) {
		HashMap<String, Object> map = memServ.getDependantAllClaims(dependantId);
		return new ResponseEntity<Object>(genRes.genResponse(map, "Success", HttpStatus.OK.value()), HttpStatus.OK);
	}

	@GetMapping("/user/dependant/get/claim/{id}")
	public ResponseEntity<Object> getClaim(@PathVariable(value = "id") String claimId) {
		Claim claim = memServ.getClaim(claimId);
		return new ResponseEntity<Object>(claim, HttpStatus.OK);
	}

}
