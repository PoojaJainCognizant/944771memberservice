package com.cognizant.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.configuration.GenerateResponse;
import com.cognizant.dao.RegsiterUserDao;
import com.cognizant.exceptions.EntityNotFoundException;
import com.cognizant.service.MemberService;
import com.cognizant.vm.UserVM;

@RestController
@RequestMapping("/api/member")
public class RegisterController {

	Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	private MemberService memberService;

	@Autowired
	private GenerateResponse genRes;

	@PostMapping("/register")
	public ResponseEntity<Object> registerUser(@RequestBody @Valid RegsiterUserDao regUserDao, HttpServletRequest request) throws EntityNotFoundException {
			UserVM newUser = memberService.registerNewUser(regUserDao);
			return new ResponseEntity<Object>(genRes.genResponse(newUser, "success", HttpStatus.OK.value()),
					HttpStatus.OK);
	}

}
