package com.cognizant.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.configuration.GenerateResponse;
import com.cognizant.dao.CountryDao;
import com.cognizant.model.Country;
import com.cognizant.model.State;
import com.cognizant.service.CountryService;

@RestController
@RequestMapping("/api/member/country")
public class CountryController {

	@Autowired
	private GenerateResponse genRes;

	@Autowired
	private CountryService countryServ;

	@PostMapping("/create")
	public ResponseEntity<Object> saveCountry(@RequestBody CountryDao countryDao, HttpServletRequest request) {

		Country country = countryServ.saveCountry(countryDao);
		return new ResponseEntity<Object>(country, HttpStatus.OK);
	}

	@GetMapping("/getAll")
	public ResponseEntity<Object> getAllCountries() {
		List<HashMap<String, Object>> countries = countryServ.getAllCountries();
		return new ResponseEntity<Object>(genRes.genResponse(countries, "Success", HttpStatus.OK.value()),
				HttpStatus.OK);
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Object> getCountry(@PathVariable(value = "id") String id) {
		Country country = countryServ.getCountry(id);
		HashMap<String, Object> data = new HashMap<>();
		data.put("countryId", country.getId());
		List<HashMap<String, Object>> stateList = new ArrayList<>();
		for (State state : country.getStates()) {
			HashMap<String, Object> stateData = new HashMap<>();
			stateData.put("id", state.getId());
			stateData.put("name", state.getName());
			stateList.add(stateData);
		}
		data.put("states", stateList);
		return new ResponseEntity<Object>(genRes.genResponse(data, "Success", HttpStatus.OK.value()), HttpStatus.OK);
	}

}
