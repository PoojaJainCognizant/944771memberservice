package com.cognizant.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import com.cognizant.model.Country;
import com.cognizant.model.State;
import com.cognizant.model.User;
import com.cognizant.repository.UserRepository;

import java.util.Date;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;

//@RunWith(SpringRunner.class)
//@SpringBootTest
@Component
public class UserRegistrationTest {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private UserRepository userRepo;
	
	@Test
	public void registerUser() {
		User newUser = new User();
		newUser.setId(this.generateUniqueId());
		newUser.setFirstName("Pooja");
		newUser.setLastName("Jain");
		newUser.setAddress("Nagpur");
		newUser.setCountry(this.findCountry());
		newUser.setState(this.findState());
		newUser.setEmail("poojajain@gmail.com");
		newUser.setPan("ABCDEFGH1234");
		Long longNo = (long) 987065436;
		newUser.setContactNo(longNo);
		Date date = new Date();
		newUser.setDateOfBirth(date);
		userRepo.save(newUser);
	}
	
	public Country findCountry() {
		Query query = new Query(Criteria.where("name").is("India"));
		Country country = mongoTemplate.findOne(query, Country.class);
		return country;
	}
	
	public State findState() {
		Query query = new Query(Criteria.where("name").is("Maharashtra"));
		State state = mongoTemplate.findOne(query, State.class);
		return state;
	}
	
	public String generateUniqueId() {
		Random random = new Random();
		Integer ranId = random.nextInt(900)+100;
		String id = "R"+ranId.toString();
		return id;
	}
}
