FROM openjdk:latest
EXPOSE 9002
ADD target/memberservice-0.0.1-SNAPSHOT.jar memberservice-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java -jar /memberservice-0.0.1-SNAPSHOT.jar" ]